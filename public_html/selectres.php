<?php session_start(); 

$_SESSION['Resname'] = $_GET['Residentname'];

?> 
<html>
<H2>Please answer the following questions.  You may skip any questions if you choose.</H2>
<form  action = "thanks.php" method="get" onReset="window.location='http://www.usask.ca'">
<br>






<h3>Demographic Information (optional):</h3>

<br>
Please enter your age: (this information will not be seen by the doctor you are evaluating)
<input type="text" name="age" >
<br>
<br>
Please enter your gender: (this information will not be seen by the doctor you are evaluating)
<table>
<tr>
<td><input type="radio" name="gender" value="male" />Male<br /></td>
<td><input type="radio" name="gender" value="female" />Female<br /></td>
</table>
<br>

What problem were you seeing the doctor for? (this information will not be seen by the doctor you are evaluating)
<br>
<table>
<textarea cols="40" rows="5" name="Chiefcomplaint">
</textarea>
<br>
<br>

<h3>Survey Questions (also optional):</h3>
<br>
Question 1: <?php 
echo $_GET['Residentname'];
?> listened to what I was saying.<br>
<table>

<tr>
<td><input type="radio" name="Q1" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="Q1" value="2" />Disagree<br /></td>
<td><input type="radio" name="Q1" value="3" />Neutral<br /></td>
<td><input type="radio" name="Q1" value="4" />Agree<br /></td>
<td><input type="radio" name="Q1" value="5" />Strongly agree<br /></td></tr>
</table>

<br>

Question 2: <?php echo $_GET['Residentname'];?> spent enough time with me.
<table>

<tr>
<td><input type="radio" name="Q2" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="Q2" value="2" />Disagree<br /></td>
<td><input type="radio" name="Q2" value="3" />Neutral<br /></td>
<td><input type="radio" name="Q2" value="4" />Agree<br /></td>
<td><input type="radio" name="Q2" value="5" />Strongly agree<br /></td></tr>
</table>

<br>

Question 3: <?php 
echo $_GET['Residentname'];
?> was knowledgable.
<table>

<tr>
<td><input type="radio" name="Q3" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="Q3" value="2" />Disagree<br /></td>
<td><input type="radio" name="Q3" value="3" />Neutral<br /></td>
<td><input type="radio" name="Q3" value="4" />Agree<br /></td>
<td><input type="radio" name="Q3" value="5" />Strongly agree<br /></td></tr>
</table>
<br>

Question 4: What did you think <?php 
echo $_GET['Residentname'];
?> did well?
<br>
<textarea cols="70" rows="5" name="Q4">
</textarea>
<br>
<br>
Question 5: What would you like <?php 
echo $_GET['Residentname'];
?> to do more?
<br>
<textarea cols="70" rows="5" name="Q5">
</textarea>


<br>
<br>
Question 6: What would you like <?php 
echo $_GET['Residentname'];
?> to do less?
<br>
<textarea cols="70" rows="5" name="Q6">
</textarea>

<br>
<br>
Question 7: What would you like <?php 
echo $_GET['Residentname'];
?> to stop doing?
<br>
<textarea cols="70" rows="5" name="Q7">
</textarea>

<br>
<br>
Question 8: Please enter in any other comments regarding your visit today.
<br>
<textarea cols="70" rows="5" name="Q8">

</textarea>
<br>
<br>
<input type="submit" value="Submit" />
<input type="reset" value="Cancel" />
</form>


</html>