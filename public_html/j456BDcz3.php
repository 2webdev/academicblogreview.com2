<?php session_start(); 


?> 
<html>
<H2>Dear Dr. Begin, thank-you for participating in this survey.</H2>
<form  action = "thanks8.php" method="get" onReset="window.location='http://www.usask.ca'">
<br>



<h3>Question 1:</h3>
<br>
<table><tr><td>Dr. Begin listened to what I was saying:</td><td>4.8/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ1" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ1" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ1" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ1" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ1" value="5" />Strongly agree<br/></td>
<td><input type="radio" name="RQ1" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q1comment">
</textarea>
<br>
<br>


<h3>Question 2:</h3>
<br>
<table><tr><td>Dr. Begin spent enough time with me.</td><td>4.6/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ2" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ2" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ2" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ2" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ2" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ2" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q2comment">
</textarea>
<br>
<br>

<h3>Question 3:</h3>
<br>
<table><tr><td>Dr. Begin was knowledgable.</td><td>4.8/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ3" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ3" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ3" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ3" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ3" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ3" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q3comment">
</textarea>
<br>
<br>

<h3>Question 4:</h3>
<br>
 What do you think Dr. Begin did well?
<br>
<br>
She was very knowledgeable and listened and asked me good questions.  I liked that she followed up with me on my current medications and things to think about regarding them.  I felt like she wanted to make sure I had all info I needed before I left the office, which made it easy for me to ask questions because her questions brought up things I wouldn't have thought to ask.  Checked her facts and suggestions with a book, and the computer, and did comparisons, and thorough discussion so I could understand, this was not explained to me before.  Made me feel comfortable and aware of what she was doing while examinging me. She listened to my concerns, She took time to explain things to me in language I could understand. She was very thorough and gentle during the exam. I felt very comfortable with her. She is and excellent doctor.
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ4" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ4" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ4" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ4" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ4" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ4" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q4comment">
</textarea>
<br>
<br>


<h3>Question 5:</h3>
<br>
What would you like Dr. Begin to do more?
<br>
<br>Can't think of anything.  She was very thorough, helpful and super pleasant! Just a follow up with me.  I don't feel she left anything out.  
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ5" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ5" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ5" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ5" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ5" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ5" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q5comment">
</textarea>
<br>
<br>


<h3>Question 6:</h3>
<br>
What would you like Dr. Begin to do less?
<br>
<br>I felt my appointment was great.  Wouldn't ask for anything less.  It was just right.  She was respectful and courteous as well as informative. 
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ6" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ6" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ6" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ6" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ6" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ6" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q6comment">
</textarea>
<br>

<br>

<h3>Question 7:</h3>
<br>
What would you like Dr. Begin to stop doing?
<br>
<br>No responses.
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ7" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ7" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ7" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ7" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ7" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ7" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q7comment">
</textarea>
<br>
<br>

<h3>Question 8:</h3>
<br>
Other comments:
<br>
<br>Thank you Dr. Begin.  You were very helpful and I felt quite comfortable throughout the entire appointment.  You made me feel at ease and I really appreciated that you asked me questions.  You were friendly and open to discussing anything I had concerns with.  Thanks!  One of the most refreshing and positive Dr's appt I have had in a long time! She was easy to discuss with, approachable, not rushed. I came away from my visit feeling relieved. I would refer any anxious female patient to see her. She was very professional.


<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ8" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ8" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ8" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ8" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ8" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ8" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q8comment">
</textarea>
<br>
<br>

<h3>Question 9:  Will you change the way you practice as a result of the feedback you have received?</h3>
<br>


<table>

<tr>
<td><input type="radio" name="RQ9" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ9" value="2" />No<br /></td>
<td><input type="radio" name="RQ9" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q9comment">
</textarea>
<br>
<br>
<h3>Question 10:  Do you feel that the value of the feedback is limited by not knowing about the patient it came from?</h3>

<br>
<table>

<tr>
<td><input type="radio" name="RQ10" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ10" value="2" />No<br /></td>
<td><input type="radio" name="RQ10" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q10comment">
</textarea>
<br>
<br>

<h3>Question 11:  Do you think that feedback from patients should be for informative purposes only or should it be included in a resident's evaluation?</h3>
<br>

<textarea cols="70" rows="5" name="Q11comment"></textarea>

<br>
<br>

<h3>Question 12:  In general, are you in favour of receiving feedback from patients?</h3>
<br>


<table>

<tr>
<td><input type="radio" name="RQ12" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ12" value="2" />No<br /></td>
<td><input type="radio" name="RQ12" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q12comment"></textarea>

<br>
<br>


<br>
<input type="submit" value="Submit" />
<input type="reset" value="Cancel" />
</form>


</html>