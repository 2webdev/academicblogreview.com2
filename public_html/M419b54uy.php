<?php session_start(); 


?> 
<html>
<H2>Dear Dr. Barnett, thank-you for participating in this survey.</H2>
<form  action = "thanks18.php" method="get" onReset="window.location='http://www.usask.ca'">
<br>



<h3>Question 1:</h3>
<br>
<table><tr><td>Dr. Barnett listened to what I was saying:</td><td>4.75/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ1" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ1" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ1" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ1" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ1" value="5" />Strongly agree<br/></td>
<td><input type="radio" name="RQ1" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q1comment">
</textarea>
<br>
<br>


<h3>Question 2:</h3>
<br>
<table><tr><td>Dr. Barnett spent enough time with me.</td><td>4.75/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ2" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ2" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ2" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ2" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ2" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ2" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q2comment">
</textarea>
<br>
<br>

<h3>Question 3:</h3>
<br>
<table><tr><td>Dr. Barnett was knowledgable.</td><td>4.25/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ3" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ3" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ3" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ3" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ3" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ3" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q3comment">
</textarea>
<br>
<br>

<h3>Question 4:</h3>
<br>
 What do you think Dr. Barnett did well?
<br>
<br>Pt was satisfied with her appt with the Dr. When he wasn't sure of the answer he did everything he could to find it. Easy to understand his thoughts and possible diagnosis. Did a good leg check on both legs.  Suggested a possible physiotherapy appointment if I desire to do that.  Or come back if further concerns. Assessment, discussion at appropriate level, thoughtful, analytical, friendly, good feedback, appeared not rushed, good explanations.
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ4" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ4" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ4" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ4" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ4" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ4" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q4comment">
</textarea>
<br>
<br>


<h3>Question 5:</h3>
<br>
What would you like Dr. Barnett to do more?
<br>
<br>Nothing.
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ5" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ5" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ5" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ5" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ5" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ5" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q5comment">
</textarea>
<br>
<br>


<h3>Question 6:</h3>
<br>
What would you like Dr. Barnett to do less?
<br>
<br>No comments.
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ6" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ6" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ6" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ6" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ6" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ6" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q6comment">
</textarea>
<br>

<br>

<h3>Question 7:</h3>
<br>
What would you like Dr. Barnett to stop doing?
<br>
<br>No comments.
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ7" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ7" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ7" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ7" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ7" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ7" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q7comment">
</textarea>
<br>
<br>

<h3>Question 8:</h3>
<br>
Other comments:
<br>
<br>She was very happy with the appt and she had a busy day and she was pleased that it was a quick and enough time appt and she left with a happy child with a sticker (she brought in her other daughter as well ) who is 3 1/2 yrs old. Dr. Barnett is a great doctor, though he does seem a bit timid and shy... something I'm hoping he will over come in time. I felt comfortable with his suggestions, physical check and his personality. 



<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ8" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ8" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ8" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ8" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ8" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ8" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q8comment">
</textarea>
<br>
<br>

<h3>Question 9:  Will you change the way you practice as a result of the feedback you have received?</h3>
<br>


<table>

<tr>
<td><input type="radio" name="RQ9" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ9" value="2" />No<br /></td>
<td><input type="radio" name="RQ9" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q9comment">
</textarea>
<br>
<br>
<h3>Question 10:  Do you feel that the value of the feedback is limited by not knowing about the patient it came from?</h3>

<br>
<table>

<tr>
<td><input type="radio" name="RQ10" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ10" value="2" />No<br /></td>
<td><input type="radio" name="RQ10" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q10comment">
</textarea>
<br>
<br>

<h3>Question 11:  Do you think that feedback from patients should be for informative purposes only or should it be included in a resident's evaluation?</h3>
<br>

<textarea cols="70" rows="5" name="Q11comment"></textarea>

<br>
<br>

<h3>Question 12:  In general, are you in favour of receiving feedback from patients?</h3>
<br>


<table>

<tr>
<td><input type="radio" name="RQ12" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ12" value="2" />No<br /></td>
<td><input type="radio" name="RQ12" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q12comment"></textarea>

<br>
<br>


<br>
<input type="submit" value="Submit" />
<input type="reset" value="Cancel" />
</form>


</html>