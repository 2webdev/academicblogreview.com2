<?php session_start(); 


?> 
<html>
<H2>Dear Dr. Ryan, thank-you for participating in this survey.</H2>
<form  action = "thanks10.php" method="get" onReset="window.location='http://www.usask.ca'">
<br>



<h3>Question 1:</h3>
<br>
<table><tr><td>Dr. Ryan listened to what I was saying:</td><td>4.8/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ1" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ1" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ1" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ1" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ1" value="5" />Strongly agree<br/></td>
<td><input type="radio" name="RQ1" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q1comment">
</textarea>
<br>
<br>


<h3>Question 2:</h3>
<br>
<table><tr><td>Dr. Ryan spent enough time with me.</td><td>5/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ2" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ2" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ2" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ2" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ2" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ2" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q2comment">
</textarea>
<br>
<br>

<h3>Question 3:</h3>
<br>
<table><tr><td>Dr. Rayn was knowledgable.</td><td>4.8/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ3" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ3" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ3" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ3" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ3" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ3" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q3comment">
</textarea>
<br>
<br>

<h3>Question 4:</h3>
<br>
What do you think Dr. Ryan did well?
<br><br>She answered my questions fully and explained the reason for a particular action. She definitely knew what she was doing. Communication skill were very good. Knowledgible about subjects disacussed. Suggested alternative meds for breathing difficulties. she explained everything really well, a good bedside manner, and very gentle. Asked appropriate questions, knew what to ask.
<br>
<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ4" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ4" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ4" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ4" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ4" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ4" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q4comment">
</textarea>
<br>
<br>


<h3>Question 5:</h3>
<br>
What would you like Dr. Ryan to do more?
<br><br>Ask questions. 
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ5" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ5" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ5" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ5" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ5" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ5" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q5comment">
</textarea>
<br>
<br>


<h3>Question 6:</h3>
<br>
What would you like Dr. Ryan to do less?
<br><br>Nothing. 

<br>
<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ6" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ6" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ6" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ6" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ6" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ6" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q6comment">
</textarea>
<br>

<br>

<h3>Question 7:</h3>
<br>What would you like Dr. Ryan to stop doing?
<br><br>No responses.
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ7" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ7" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ7" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ7" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ7" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ7" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q7comment">
</textarea>
<br>
<br>

<h3>Question 8:</h3>
<br>Other comments:
<br><br>I appreciated the communication skills of Dr. Ryan.  It was very well done.  Good bedside manner!




<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ8" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ8" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ8" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ8" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ8" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ8" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q8comment">
</textarea>
<br>
<br>

<h3>Question 9:  Will you change the way you practice as a result of the feedback you have received?</h3>
<br>
<table>

<tr>
<td><input type="radio" name="RQ9" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ9" value="2" />No<br /></td>
<td><input type="radio" name="RQ9" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q9comment">
</textarea>
<br>
<br>
<h3>Question 10:  Do you feel that the value of the feedback is limited by not knowing about the patient it came from?</h3>
<br>
<table>

<tr>
<td><input type="radio" name="RQ10" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ10" value="2" />No<br /></td>
<td><input type="radio" name="RQ10" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q10comment">
</textarea>
<br>
<br>

<h3>Question 11:  Do you think that feedback from patients should be for informative purposes only or should it be included in a resident's evaluation?</h3>
<br>
<textarea cols="70" rows="5" name="Q11comment"></textarea>

<br>
<br>

<h3>Question 12:  In general, are you in favour of receiving feedback from patients?</h3>
<br>
<table>

<tr>
<td><input type="radio" name="RQ12" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ12" value="2" />No<br /></td>
<td><input type="radio" name="RQ12" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q12comment"></textarea>

<br>
<br>


<br>
<input type="submit" value="Submit" />
<input type="reset" value="Cancel" />
</form>


</html>