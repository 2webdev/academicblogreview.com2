<html>



<?php
class pointLocation {
    var $pointOnVertex = true; // Check if the point sits exactly on one of the vertices

    function pointLocation() {
    }
    
    
        function pointInPolygon($point, $polygon, $pointOnVertex = true) {
        $this->pointOnVertex = $pointOnVertex;
        
        // Transform string coordinates into arrays with x and y values
        $point = $this->pointStringToCoordinates($point);
        $vertices = array(); 
        foreach ($polygon as $vertex) {
            $vertices[] = $this->pointStringToCoordinates($vertex); 
        }
        
        // Check if the point sits exactly on a vertex
        if ($this->pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true) {
            return "vertex";
        }
        
        // Check if the point is inside the polygon or on the boundary
        $intersections = 0; 
        $vertices_count = count($vertices);
    
        for ($i=1; $i < $vertices_count; $i++) {
            $vertex1 = $vertices[$i-1]; 
            $vertex2 = $vertices[$i];
            if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) { // Check if point is on an horizontal polygon boundary
                return "boundary";
            }
            if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y']) { 
                $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x']; 
                if ($xinters == $point['x']) { // Check if point is on the polygon boundary (other than horizontal)
                    return "boundary";
                }
                if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                    $intersections++; 
                }
            } 
        } 
        // If the number of edges we passed through is even, then it's in the polygon. 
        if ($intersections % 2 != 0) {
            return "inside";
        } else {
            return "outside";
        }
    }

    
    
    function pointOnVertex($point, $vertices) {
        foreach($vertices as $vertex) {
            if ($point == $vertex) {
                return true;
            }
        }
    
    }
        
    
    function pointStringToCoordinates($pointString) {
        $coordinates = explode(" ", $pointString);
        return array("x" => $coordinates[0], "y" => $coordinates[1]);
    }
    
    
}


$dewdneyeast= array("50.45978899999999 -104.571991" , "50.46843 -104.525124" , "50.45738999999999 -104.525208" , "50.447788 -104.525383" , "50.447216 -104.572166" , "50.45978899999999 -104.571991" );

$arcolaeast= array("50.44701 -104.561951" , "50.447094 -104.549248" , "50.44761299999999 -104.526505" , "50.441437 -104.526505" ,"50.441437 -104.523026" , "50.418743 -104.52388" , "50.407833 -104.528648" , "50.42738700000001 -104.571907" , "50.43919400000001 -104.561264" , "50.44701 -104.561951" );

$northcentral= array("50.469688 -104.60656" , "50.455208 -104.606537" , "50.455154 -104.618256" , "50.451683 -104.618256" , "50.449429 -104.630638" , "50.447983 -104.642654" , "50.45097400000001 -104.64267" , "50.45539899999999 -104.643578" , "50.466465 -104.643661" , "50.469551 -104.641815" , "50.469593 -104.624329" , "50.469578 -104.615639" , "50.469578 -104.60656" , "50.469688 -104.60656" );


$boothill = array("50.437656 -104.583611" , "50.437706 -104.579369" , "50.440437 -104.572388" , "50.44162800000001 -104.572388" , "50.437096 -104.563568" , "50.427708 -104.572815" , "50.431377 -104.579544" , "50.431393 -104.583549" , "50.437664 -104.583611" );

$downtown = array("50.455135 -104.606499", "50.455101 -104.618019", "50.447231 -104.618057", "50.447212 -104.606384", "50.455135 -104.606499");
$willowgrove = array("0 0", "1 1");
$lakeview = array("50.43980400000001 -104.644592","50.439915 -104.634033","50.438877 -104.630257","50.435272 -104.627136","50.435711 -104.618256","50.418854 -104.618279","50.418816 -104.642136","50.42240899999999 -104.641685","50.42661999999999 -104.641342","50.435261 -104.644257","50.43980400000001 -104.644592");
$albertpark = array("50.418362 -104.618423" , "50.396702 -104.61834" , "50.396866 -104.644432" , "50.41333000000001 -104.643913" , "50.418655 -104.642212" , "50.418781 -104.618446" , "50.418362 -104.618423");
$regentpark = array("50.484097 -104.652466","50.484108 -104.641373", "50.471031 -104.641365", "50.471020 -104.652588", "50.484097 -104.652466" );

$hillsdale = array("50.423283 -104.60186" , "50.428207 -104.60083" , "50.42498000000001 -104.596107" , "50.42282100000001 -104.594566" , "50.420658 -104.593964" , "50.418362 -104.594223" , "50.41655699999999 -104.595078" , "50.413441 -104.596191" , "50.410324 -104.595078" , "50.408024 -104.589584" , "50.40539900000001 -104.592674" , "50.408188 -104.599586" , "50.40813399999999 -104.608986" , "50.415653 -104.609024" , "50.41568400000001 -104.617867" , "50.42320300000001 -104.617653" , "50.42331300000001 -104.601814" );


$whitmorepark = array("50.41532099999999 -104.617783" , "50.41538200000001 -104.609329" , "50.40786000000001 -104.609367" , "50.40787099999999 -104.599442" , "50.405128 -104.592743" , "50.400829 -104.597008" , "50.39703 -104.602287" , "50.39698 -104.617813" , "50.415352 -104.617783" );


$neighborhoods = array($downtown,$willowgrove);
echo "<table>";



$row = 1;
if (($handle = fopen("regina.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
 $pointLocation = new pointLocation();
   $points = array($data[1]." ".$data[2]);

 $outputstring = $pointLocation->pointInPolygon($points[0], $albertpark);
if ($outputstring == "inside") {
 echo "<tr><td>".$data[0]."</td><td>".$data[1]."</td><td>".$data[2]."</td><td>"."Albert Park"."</td></tr>";
}

 $outputstring = $pointLocation->pointInPolygon($points[0], $arcolaeast);
if ($outputstring == "inside") {
 echo "<tr><td>".$data[0]."</td><td>".$data[1]."</td><td>".$data[2]."</td><td>"."Arcola East"."</td></tr>";
}

 $outputstring = $pointLocation->pointInPolygon($points[0], $boothill);
if ($outputstring == "inside") {
 echo "<tr><td>".$data[0]."</td><td>".$data[1]."</td><td>".$data[2]."</td><td>"."Boot Hill"."</td></tr>";
}
$outputstring = $pointLocation->pointInPolygon($points[0], $dewdneyeast);
if ($outputstring == "inside") {
 echo "<tr><td>".$data[0]."</td><td>".$data[1]."</td><td>".$data[2]."</td><td>"."Dewdney East"."</td></tr>";
}

 $outputstring = $pointLocation->pointInPolygon($points[0], $downtown);
if ($outputstring == "inside") {
 echo "<tr><td>".$data[0]."</td><td>".$data[1]."</td><td>".$data[2]."</td><td>"."Downtown"."</td></tr>";
}
 $outputstring = $pointLocation->pointInPolygon($points[0], $regentpark);
if ($outputstring == "inside") {
 echo "<tr><td>".$data[0]."</td><td>".$data[1]."</td><td>".$data[2]."</td><td>"."Regent Park"."</td></tr>";
}
 $outputstring = $pointLocation->pointInPolygon($points[0], $lakeview);
if ($outputstring == "inside") {
 echo "<tr><td>".$data[0]."</td><td>".$data[1]."</td><td>".$data[2]."</td><td>"."Lakeview"."</td></tr>";
}
 $outputstring = $pointLocation->pointInPolygon($points[0], $northcentral);
if ($outputstring == "inside") {
 echo "<tr><td>".$data[0]."</td><td>".$data[1]."</td><td>".$data[2]."</td><td>"."North Central"."</td></tr>";
}
  $outputstring = $pointLocation->pointInPolygon($points[0], $whitmorepark);
if ($outputstring == "inside") {
 echo "<tr><td>".$data[0]."</td><td>".$data[1]."</td><td>".$data[2]."</td><td>"."Whitmore Park"."</td></tr>";
}
 $outputstring = $pointLocation->pointInPolygon($points[0], $hillsdale);
if ($outputstring == "inside") {
 echo "<tr><td>".$data[0]."</td><td>".$data[1]."</td><td>".$data[2]."</td><td>"."Hillsdale"."</td></tr>";
}
     
     


      
        }
    }
    fclose($handle);


echo "</table>";

?>
</html>