<?php session_start(); 


?> 
<html>
<H2>Dear Dr. Schindel, thank-you for participating in this survey.</H2>
<form  action = "thanks22.php" method="get" onReset="window.location='http://www.usask.ca'">
<br>



<h3>Question 1:</h3>
<br>
<table><tr><td>Dr. Schindel listened to what I was saying:</td><td>4.7/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ1" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ1" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ1" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ1" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ1" value="5" />Strongly agree<br/></td>
<td><input type="radio" name="RQ1" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q1comment">
</textarea>
<br>
<br>


<h3>Question 2:</h3>
<br>
<table><tr><td>Dr. Schindel spent enough time with me.</td><td>4.3/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ2" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ2" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ2" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ2" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ2" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ2" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q2comment">
</textarea>
<br>
<br>

<h3>Question 3:</h3>
<br>
<table><tr><td>Dr. Schindel was knowledgable.</td><td>4/5</td></tr></table>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ3" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ3" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ3" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ3" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ3" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ3" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q3comment">
</textarea>
<br>
<br>

<h3>Question 4:</h3>
<br>
What do you think Dr. Schindel did well?
<br><br>Dr Schindel introduced himself and his manner put me at ease.  Spent time listening to and taking notes about what I was at the clinic for.  He asked a lot of questions as to see why and where these headaches were coming from.  Asked questions.


<br>
<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ4" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ4" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ4" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ4" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ4" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ4" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q4comment">
</textarea>
<br>
<br>


<h3>Question 5:</h3>
<br>
What would you like Dr. Schindel to do more?
<br><br>He is a little quiet/shy but for the most part did verbalize what he thought.
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ5" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ5" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ5" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ5" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ5" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ5" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q5comment">
</textarea>
<br>
<br>


<h3>Question 6:</h3>
<br>
What would you like Dr. Schindel to do less?
<br><br>Nothing.

<br>
<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ6" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ6" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ6" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ6" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ6" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ6" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q6comment">
</textarea>
<br>

<br>

<h3>Question 7:</h3>
<br>What would you like Dr. Schindel to stop doing?
<br><br>Nothing.
<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ7" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ7" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ7" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ7" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ7" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ7" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q7comment">
</textarea>
<br>
<br>

<h3>Question 8:</h3>
<br>Other comments:
<br><br>Part of the visit included a digital exam for prostate enlargement.  This is an unpleasant and awkward procedure for patient and doctor alike.  Dr Schindel did the procedure in a courteous and professional way which lessened my discomfort about this part of the visit.  He was friendly and polite and seemed to be very capable and knowledgeable in what he was doing. i felt comfortable with him.  He was very friendly and easy to talk to.






<br>

<br>
Do you agree with this assessment?
<table>

<tr>
<td><input type="radio" name="RQ8" value="1" />Strongly disagree<br /></td>
<td><input type="radio" name="RQ8" value="2" />Disagree<br /></td>
<td><input type="radio" name="RQ8" value="3" />Neutral<br /></td>
<td><input type="radio" name="RQ8" value="4" />Agree<br /></td>
<td><input type="radio" name="RQ8" value="5" />Strongly agree<br /></td>
<td><input type="radio" name="RQ8" value="6" />N/A<br /></td></tr>
</table>
<br>
<textarea cols="70" rows="5" name="Q8comment">
</textarea>
<br>
<br>

<h3>Question 9:  Will you change the way you practice as a result of the feedback you have received?</h3>
<br>
<table>

<tr>
<td><input type="radio" name="RQ9" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ9" value="2" />No<br /></td>
<td><input type="radio" name="RQ9" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q9comment">
</textarea>
<br>
<br>
<h3>Question 10:  Do you feel that the value of the feedback is limited by not knowing about the patient it came from?</h3>
<br>
<table>

<tr>
<td><input type="radio" name="RQ10" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ10" value="2" />No<br /></td>
<td><input type="radio" name="RQ10" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q10comment">
</textarea>
<br>
<br>

<h3>Question 11:  Do you think that feedback from patients should be for informative purposes only or should it be included in a resident's evaluation?</h3>
<br>
<textarea cols="70" rows="5" name="Q11comment"></textarea>

<br>
<br>

<h3>Question 12:  In general, are you in favour of receiving feedback from patients?</h3>
<br>
<table>

<tr>
<td><input type="radio" name="RQ12" value="1" />Yes<br /></td>
<td><input type="radio" name="RQ12" value="2" />No<br /></td>
<td><input type="radio" name="RQ12" value="3" />Not sure<br /></td>
</table>
<br>
<textarea cols="70" rows="5" name="Q12comment"></textarea>

<br>
<br>


<br>
<input type="submit" value="Submit" />
<input type="reset" value="Cancel" />
</form>


</html>